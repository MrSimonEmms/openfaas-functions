/**
 * handler
 */

/* Node modules */

/* Third-party modules */

/* Files */
const factory = require('./common/factory');
const methods = require('./methods');

module.exports = async (event, context) => {
  const tokenFactory = async () =>
    methods.exchangeRefreshToken(
      await event.getSecret('google-calendar-client-id'),
      await event.getSecret('google-calendar-client-secret'),
      event.body.refreshToken,
    );

  return factory(event, context, methods, tokenFactory);
};
