/**
 * factory
 */

/* Node modules */

/* Third-party modules */

/* Files */

module.exports = async (
  event,
  context,
  methods,
  tokenFactory,
  exclude = ['exchangeRefreshToken'],
) => {
  context.httpHeaders = {
    'content-type': 'application/json',
  };

  let token;

  try {
    token = await tokenFactory(event, context);
  } catch (err) {
    console.error('Invalid refreshToken received');
    context.httpStatus = 401;
    return {};
  }

  const { args = {}, method } = event.body;

  if (exclude.includes(method) === false && method in methods) {
    try {
      return await methods[method](token, args);
    } catch (err) {
      if (err.code === 'INVALID') {
        console.error('Validation error');
        context.httpStatus = 400;

        return {
          message: err.message,
          info: err.info,
        };
      }

      throw err;
    }
  }

  context.httpStatus = 404;

  return {
    message: `Method unknown: ${method}`,
  };
};
