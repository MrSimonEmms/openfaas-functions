/**
 * methods.spec
 */

/* Node modules */
const qs = require('querystring');

/* Third-party modules */
const axios = require('axios');

/* Files */
const methods = require('../../functions/google-calendar/methods');

describe('methods', () => {
  describe('#deleteCalendarEvent', () => {
    it('should error if calendarId not set', async () => {
      const token = 'some-token';
      const data = {
        eventId: 'some-eventId',
      };

      try {
        await methods.deleteCalendarEvent(token, data);
        expect('called').toBe(false);
      } catch (err) {
        expect(err.message).toBe('calendarId is a required option');
        expect(err.code).toBe('INVALID');
      }
    });

    it('should error if eventId not set', async () => {
      const token = 'some-token2';
      const data = {
        calendarId: 'some-calendarId',
      };

      try {
        await methods.deleteCalendarEvent(token, data);
        expect('called').toBe(false);
      } catch (err) {
        expect(err.message).toBe('eventId is a required option');
        expect(err.code).toBe('INVALID');
      }
    });

    it('should call the endpoint', async () => {
      const token = 'some-token2';
      const data = {
        calendarId: 'some-calendar2',
        eventId: 'some-eventId2',
      };

      axios.delete.mockResolvedValueOnce({});

      expect(await methods.deleteCalendarEvent(token, data)).toEqual({
        deleted: true,
        eventId: data.eventId,
      });
    });
  });

  describe('#exchangeRefreshToken', () => {
    it('should exchange the refresh token', async () => {
      const clientId = 'some-client-id';
      const clientSecret = 'some-client-secret';
      const refreshToken = 'some-refresh-token';
      const accessToken = 'some-access-token';

      axios.post.mockResolvedValueOnce({
        data: {
          access_token: accessToken,
        },
      });

      expect(await methods.exchangeRefreshToken(clientId, clientSecret, refreshToken));

      expect(axios.post).toBeCalledWith(
        'https://www.googleapis.com/oauth2/v4/token',
        {
          client_id: clientId,
          client_secret: clientSecret,
          refresh_token: refreshToken,
          grant_type: 'refresh_token',
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
    });
  });

  describe('#getCalendars', () => {
    it('should return the data object', async () => {
      const data = 'some-data';
      const token = 'some-token';

      axios.get.mockResolvedValueOnce({
        data,
      });

      expect(await methods.getCalendars(token)).toBe(data);

      expect(axios.get).toBeCalledWith(
        'https://www.googleapis.com/calendar/v3/users/me/calendarList',
        {
          headers: {
            authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
          },
        },
      );
    });
  });

  describe('#getCalendarEvents', () => {
    it('should error if calendarId not set', async () => {
      try {
        await methods.getCalendarEvents('', {});
        expect('called').toBe(false);
      } catch (err) {
        expect(err.message).toBe('calendarId is a required option');
        expect(err.code).toBe('INVALID');
      }
    });

    it('should call the input with the default values', async () => {
      const token = 'some-token';
      const opts = {
        calendarId: 'some-calendarId',
      };
      const optsCopy = {
        ...opts,
      };
      const data = {
        items: [
          {
            some: 'input',
          },
        ],
      };

      axios.get.mockResolvedValueOnce({
        data,
      });

      expect(await methods.getCalendarEvents(token, opts)).toBe(data.items);

      const { calendarId } = optsCopy;
      delete optsCopy.calendarId;

      const qsParams = qs.stringify({
        alwaysIncludeEmail: false,
        orderBy: 'startTime',
        showDeleted: false,
        singleEvents: true,
        ...optsCopy,
      });

      expect(axios.get).toBeCalledWith(
        `https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events?${qsParams}`,
        {
          headers: {
            authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
          },
        },
      );
    });

    it('should call the input with set values and handle empty result', async () => {
      const token = 'some-token2';
      const opts = {
        calendarId: 'some-calendarId2',
        alwaysIncludeEmail: true,
        orderBy: 'startTime2',
        showDeleted: true,
        singleEvents: false,
        hello: 'world',
      };
      const optsCopy = {
        ...opts,
      };
      const data = {};

      axios.get.mockResolvedValueOnce({
        data,
      });

      expect(await methods.getCalendarEvents(token, opts)).toEqual([]);

      const { calendarId } = optsCopy;
      delete optsCopy.calendarId;

      const qsParams = qs.stringify({
        ...optsCopy,
      });

      expect(axios.get).toBeCalledWith(
        `https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events?${qsParams}`,
        {
          headers: {
            authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
          },
        },
      );
    });
  });
});
