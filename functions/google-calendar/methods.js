/**
 * methods
 */

/* Node modules */
const qs = require('querystring');

/* Third-party modules */
const axios = require('axios');

/* Files */
const ValidationError = require('./common/validationError');

module.exports = {
  async deleteCalendarEvent(token, { calendarId, eventId }) {
    if (!calendarId) {
      throw new ValidationError('calendarId is a required option');
    }

    if (!eventId) {
      throw new ValidationError('eventId is a required option');
    }

    await axios.delete(
      `https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events/${eventId}`,
      {
        headers: {
          authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      },
    );

    return {
      eventId,
      deleted: true,
    };
  },

  async exchangeRefreshToken(clientId, clientSecret, refreshToken) {
    const { data } = await axios.post(
      'https://www.googleapis.com/oauth2/v4/token',
      {
        client_id: clientId,
        client_secret: clientSecret,
        refresh_token: refreshToken,
        grant_type: 'refresh_token',
      },
      {
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );

    return data.access_token;
  },

  async getCalendars(token) {
    const { data } = await axios.get(
      'https://www.googleapis.com/calendar/v3/users/me/calendarList',
      {
        headers: {
          authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      },
    );

    return data;
  },

  async getCalendarEvents(token, opts) {
    const { calendarId } = opts;

    if (!calendarId) {
      throw new ValidationError('calendarId is a required option');
    }

    // eslint-disable-next-line no-param-reassign
    delete opts.calendarId;

    const qsParams = qs.stringify({
      alwaysIncludeEmail: false,
      orderBy: 'startTime',
      showDeleted: false,
      singleEvents: true,
      ...opts,
    });

    const { data } = await axios.get(
      `https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events?${qsParams}`,
      {
        headers: {
          authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      },
    );

    const { items = [] } = data;

    return items;
  },
};
