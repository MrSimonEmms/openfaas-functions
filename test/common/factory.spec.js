/**
 * factory.spec
 */

/* Node modules */

/* Third-party modules */

/* Files */
const factory = require('../../common/factory');
const ValidationError = require('../../common/validationError');

describe('factory', () => {
  let fnEvent;
  let fnContext;

  beforeEach(() => {
    fnEvent = {
      body: {},
      getSecret: jest.fn(),
    };

    fnContext = {
      httpHeaders: {},
    };
  });

  it('should fail to get a refresh token', async () => {
    let factoryCalled = false;
    const tokenFactory = (event, context) => {
      expect(event).toBe(fnEvent);
      expect(context).toBe(fnContext);
      factoryCalled = true;

      throw new Error('some error');
    };

    expect(await factory(fnEvent, fnContext, {}, tokenFactory)).toEqual({});

    expect(factoryCalled).toEqual(true);
    expect(fnContext.httpHeaders).toEqual({
      'content-type': 'application/json',
    });
    expect(fnContext.httpStatus).toBe(401);
  });

  it('should return 404 with default exclude array', async () => {
    let factoryCalled = false;
    const token = 'some-token';

    const tokenFactory = (event, context) => {
      expect(event).toBe(fnEvent);
      expect(context).toBe(fnContext);
      factoryCalled = true;

      return token;
    };

    const methods = {
      exchangeRefreshToken: () => {
        throw new Error('invalid');
      },
    };

    fnEvent.body.method = 'exchangeRefreshToken';

    expect(await factory(fnEvent, fnContext, methods, tokenFactory)).toEqual({
      message: `Method unknown: ${fnEvent.body.method}`,
    });

    expect(factoryCalled).toEqual(true);
    expect(fnContext.httpHeaders).toEqual({
      'content-type': 'application/json',
    });
    expect(fnContext.httpStatus).toBe(404);
  });

  it('should return 404 with set exclude array', async () => {
    let factoryCalled = false;
    const token = 'some-token2';

    const tokenFactory = (event, context) => {
      expect(event).toBe(fnEvent);
      expect(context).toBe(fnContext);
      factoryCalled = true;

      return token;
    };

    const methods = {
      exchangeRefreshToken: () => {
        throw new Error('invalid');
      },
    };

    fnEvent.body.method = 'exchangeRefreshToken2';

    expect(await factory(fnEvent, fnContext, methods, tokenFactory, [fnEvent.body.method])).toEqual(
      {
        message: `Method unknown: ${fnEvent.body.method}`,
      },
    );

    expect(factoryCalled).toEqual(true);
    expect(fnContext.httpHeaders).toEqual({
      'content-type': 'application/json',
    });
    expect(fnContext.httpStatus).toBe(404);
  });

  it('should return 404 if method not found', async () => {
    let factoryCalled = false;
    const token = 'some-token2';

    const tokenFactory = (event, context) => {
      expect(event).toBe(fnEvent);
      expect(context).toBe(fnContext);
      factoryCalled = true;

      return token;
    };

    const methods = {};

    fnEvent.body.method = 'nonExistent';

    expect(await factory(fnEvent, fnContext, methods, tokenFactory));

    expect(factoryCalled).toEqual(true);
    expect(fnContext.httpHeaders).toEqual({
      'content-type': 'application/json',
    });
    expect(fnContext.httpStatus).toBe(404);
  });

  it('should handle a validation error', async () => {
    let factoryCalled = false;
    const token = 'some-token3';

    const tokenFactory = (event, context) => {
      expect(event).toBe(fnEvent);
      expect(context).toBe(fnContext);
      factoryCalled = true;

      return token;
    };

    const err = new ValidationError('some-validation-error');

    const methods = {
      key() {
        throw err;
      },
    };

    fnEvent.body.method = 'key';

    expect(await factory(fnEvent, fnContext, methods, tokenFactory)).toEqual({
      info: err.info,
      message: err.message,
    });

    expect(factoryCalled).toEqual(true);
    expect(fnContext.httpHeaders).toEqual({
      'content-type': 'application/json',
    });
    expect(fnContext.httpStatus).toBe(400);
  });

  it('should handle a non-validation error', async () => {
    let factoryCalled = false;
    const token = 'some-token4';

    const tokenFactory = (event, context) => {
      expect(event).toBe(fnEvent);
      expect(context).toBe(fnContext);
      factoryCalled = true;

      return token;
    };

    const err = new Error('some-error');

    const methods = {
      key2() {
        throw err;
      },
    };

    fnEvent.body.method = 'key2';

    try {
      await factory(fnEvent, fnContext, methods, tokenFactory);
    } catch (error) {
      expect(error).toBe(err);
      expect(factoryCalled).toEqual(true);
      expect(fnContext.httpHeaders).toEqual({
        'content-type': 'application/json',
      });
    }
  });

  it('should return the method result', async () => {
    let factoryCalled = false;
    const token = 'some-token5';

    const tokenFactory = (event, context) => {
      expect(event).toBe(fnEvent);
      expect(context).toBe(fnContext);
      factoryCalled = true;

      return token;
    };

    const response = 'some-response';

    const methods = {
      key3: async () => response,
    };

    fnEvent.body.method = 'key3';

    expect(await factory(fnEvent, fnContext, methods, tokenFactory)).toBe(response);

    expect(factoryCalled).toEqual(true);
    expect(fnContext.httpHeaders).toEqual({
      'content-type': 'application/json',
    });
  });
});
