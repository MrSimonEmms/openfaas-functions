# OpenFaaS Functions

A collection of OpenFaaS functions

## Development

All development can be done using [Docker Compose](https://docs.docker.com/compose/). To get all functions
running, run `docker-compose up`.

## Architecture

All are written in NodeJS and available for AMD64, ARM32V6, ARM32V7 and ARM64V8 processors unless stated
otherwise.

All images are published in the [Docker Hub](hub.docker.com/r/riggerthegeek)
prefixed with `openfaas-functions-` - eg, `docker pull riggerthegeek/openfaas-functions-distance-finder`

## Common

To DRY up the code, a series of common code snippets exist in `/common`. These are copied into each function
at build time so should be used with `require('./common/...')` inside the function. Tests are run against the common
folder directly.

## Functions

These can be run locally in docker-compose.

```shell
npm run faas-build
docker-compose up
```

- [Distance Finder](functions/distance-finder) - [port 8000](http://localhost:8000)
- [FreeAgent](functions/freeagent) - [port 8001](http://localhost:8001)
- [Google Calendar](functions/google-calendar) - [port 8002](http://localhost:8002)
