/**
 * handler
 */

/* Node modules */
const fs = require('fs').promises;
const qs = require('querystring');

/* Third-party modules */
const axios = require('axios');

/* Files */

const DISTANCE_API_URL = 'https://maps.googleapis.com/maps/api/distancematrix/json';

const callApi = async (opts) => {
  const url = `${DISTANCE_API_URL}?${qs.stringify(opts)}`;

  const { data } = await axios.get(url);

  const { status } = data;

  if (status !== 'OK') {
    throw new Error(data.error_message || status);
  }

  const [origin] = data.origin_addresses;
  const [destination] = data.destination_addresses;

  const [rows] = data.rows;
  const [elements] = rows.elements;

  /* Format the output into something usable */
  return {
    origin,
    destination,
    distance: elements.distance.value,
    duration: elements.duration.value,
  };
};

const formatOutput = (context, output, statusCode = 200) =>
  context
    .headers({
      'content-type': 'application/json',
    })
    .status(statusCode)
    .succeed(output);

const getSecret = async (secretName) => {
  try {
    return await fs.readFile(`/var/openfaas/secrets/${secretName}`, 'utf8');
  } catch {
    return fs.readFile(`/run/secrets/${secretName}`, 'utf8');
  }
};

module.exports = async (event, context) => {
  const opts = {
    origins: event.body.origin,
    destinations: event.body.destination,
    mode: event.body.mode || 'driving',
    language: event.body.language || 'en',
    units: 'metric',
    key: await getSecret('google-maps-api-key'),
  };
  const isReturnJourney = event.body.return !== false;

  const validationErr = [];
  if (!opts.origins) {
    validationErr.push('REQUIRED: origin');
  }
  if (!opts.destinations) {
    validationErr.push('REQUIRED: destination');
  }
  if (validationErr.length > 0) {
    return formatOutput(
      context,
      {
        statusCode: 400,
        message: validationErr,
      },
      400,
    );
  }

  try {
    const tasks = [callApi(opts)];
    if (isReturnJourney) {
      tasks.push(
        callApi({
          ...opts,
          origins: event.body.destination,
          destinations: event.body.origin,
        }),
      );
    }

    const [outward, inward = null] = await Promise.all(tasks);

    let { distance, duration } = outward;
    if (isReturnJourney) {
      distance += inward.distance;
      duration += inward.duration;
    }

    const output = {
      distance,
      duration,
      origin: outward.origin,
      destination: outward.destination,
      return: isReturnJourney,
    };

    return formatOutput(context, output);
  } catch (err) {
    console.error(err);

    return formatOutput(
      context,
      {
        statusCode: 400,
        message: [err.message],
      },
      400,
    );
  }
};
