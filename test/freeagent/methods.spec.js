/**
 * methods.spec
 */

/* Node modules */

/* Third-party modules */
const axios = require('axios');

/* Files */
const methods = require('../../functions/freeagent/methods');
const ValidationError = require('../../functions/freeagent/common/validationError');

describe('methods', () => {
  afterEach(() => {
    // jest.resetAllMocks();
  });

  describe('#addExpense', () => {
    it('should add an expense without the user specified', async () => {
      const token = 'some-token';
      const expense = {
        hello: 'world',
      };
      const user = {
        url: 'user-url',
      };
      const result = {
        data: 'some-result',
      };

      axios.mockResolvedValueOnce(result);

      const orig = methods.getCurrentUser;

      methods.getCurrentUser = jest.fn().mockResolvedValueOnce({
        user,
      });

      expect(
        await methods.addExpense(token, {
          expense,
        }),
      ).toBe(result.data);

      expect(methods.getCurrentUser).toBeCalledWith(token);

      expect(axios).toBeCalledWith({
        method: 'post',
        baseURL: 'https://api.freeagent.com',
        headers: {
          'user-agent': 'OpenFaaS',
          'content-type': 'application/json',
          authorization: `Bearer ${token}`,
        },
        data: {
          expense: {
            user: user.url,
            ...expense,
          },
        },
        url: '/v2/expenses',
      });

      methods.getCurrentUser = orig;
    });

    it('should add an expense with the user specified', async () => {
      const token = 'some-token';
      const expense = {
        hello: 'world2',
        user: 'user',
      };
      const result = {
        data: 'some-result2',
      };

      axios.mockResolvedValueOnce(result);

      expect(
        await methods.addExpense(token, {
          expense,
        }),
      ).toBe(result.data);

      expect(axios).toBeCalledWith({
        method: 'post',
        baseURL: 'https://api.freeagent.com',
        headers: {
          'user-agent': 'OpenFaaS',
          'content-type': 'application/json',
          authorization: `Bearer ${token}`,
        },
        data: {
          expense,
        },
        url: '/v2/expenses',
      });
    });

    it('should wrap an axios error in a ValidationError', async () => {
      const token = 'some-token';
      const expense = {
        hello: 'world2',
        user: 'user',
      };
      const result = {
        response: {
          data: 'some-result3',
        },
      };

      const oldEnv = process.env;
      jest.resetModules();
      process.env = { ...oldEnv };

      process.env.FREEAGENT_URL = 'some-freeagent-url';

      axios.mockRejectedValueOnce(result);

      try {
        expect(
          await methods.addExpense(token, {
            expense,
          }),
        ).toBe(result.data);
      } catch (err) {
        expect(err).toBeInstanceOf(ValidationError);
        expect(err.info).toBe(result.response.data);
      }

      expect(axios).toBeCalledWith({
        method: 'post',
        baseURL: process.env.FREEAGENT_URL,
        headers: {
          'user-agent': 'OpenFaaS',
          'content-type': 'application/json',
          authorization: `Bearer ${token}`,
        },
        data: {
          expense,
        },
        url: '/v2/expenses',
      });

      process.env = oldEnv;
    });
  });

  describe('#exchangeRefreshToken', () => {
    it('should exchange the refresh token', async () => {
      const clientId = 'some-client-id';
      const clientSecret = 'some-client-secret';
      const refreshToken = 'some-refresh-token';
      const accessToken = 'some-access-token';

      axios.mockResolvedValueOnce({
        data: {
          access_token: accessToken,
        },
      });

      expect(await methods.exchangeRefreshToken(clientId, clientSecret, refreshToken)).toBe(
        accessToken,
      );

      expect(axios).toBeCalledWith({
        method: 'post',
        baseURL: 'https://api.freeagent.com',
        headers: {
          'user-agent': 'OpenFaaS',
          'content-type': 'application/json',
        },
        data: {
          refresh_token: refreshToken,
          grant_type: 'refresh_token',
        },
        auth: {
          username: clientId,
          password: clientSecret,
        },
        url: '/v2/token_endpoint',
      });
    });
  });

  describe('#getCurrentUser', () => {
    it('should get the current user', async () => {
      const token = 'some-token';
      const result = {
        data: 'some-result',
      };

      axios.mockResolvedValueOnce(result);

      expect(await methods.getCurrentUser(token)).toBe(result.data);

      expect(axios).toBeCalledWith({
        method: 'get',
        baseURL: 'https://api.freeagent.com',
        headers: {
          'user-agent': 'OpenFaaS',
          'content-type': 'application/json',
          authorization: `Bearer ${token}`,
        },
        url: '/v2/users/me',
      });
    });
  });
});
