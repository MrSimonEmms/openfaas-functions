# Google Maps

Interact with Google Calendar

## Secrets

- `google-calendar-client-id`: The Google Calendar Client ID
- `google-calendar-client-secret`: The Google Calendar Client secret

## Getting a Refresh Token

Run `node ./functions/google-calendar/token.js` to run a simple Express
server that gets a refresh token for your account and go to [localhost:3000](http://localhost:3000).
You will need to specify `CLIENT_ID` and `CLIENT_SECRET` as environment
variables with credentials from [Google](https://console.developers.google.com/apis/credentials).

## Body

All endpoints require data in the following format:

```json
{
  "refreshToken": "...",
  "method": "method-name",
  "args": {}
}
```

### Methods

- deleteCalendarEvent
- getCalendars
- getCalendarEvents

### Args

Please populate the `args` object with the following key/value pairs.
Required values are marked with an asterisk (\*)

#### deleteCalendarEvent

- `calendarId`\* - string
- `eventId`\* - string

#### getCalendars

This takes no `args`.

#### getCalendarEvents

- `calendarId`\* - string
- `alwaysIncludeEmail` - boolean. Defaults to `false`
- `orderBy` - string. Defaults to `startTime`
- `showDeleted` - boolean. Defaults to `false`
- `singleEvents` - boolean. Defaults to `true`

Receives any additional query string detailed on the [API](https://developers.google.com/calendar/v3/reference/events/get)
