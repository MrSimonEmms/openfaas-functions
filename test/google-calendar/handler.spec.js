/**
 * handler.spec
 */

/* Node modules */

/* Third-party modules */

/* Files */
const factory = require('../../functions/google-calendar/common/factory');
const handler = require('../../functions/google-calendar');
const methods = require('../../functions/google-calendar/methods');

jest.mock('../../functions/google-calendar/common/factory');
jest.mock('../../functions/google-calendar/methods');

describe('google-calendar', () => {
  let fnEvent;
  let fnContext;

  beforeEach(() => {
    fnEvent = {
      body: {},
      getSecret: jest.fn(),
    };

    fnContext = {
      httpHeaders: {},
    };
  });

  it('should configure the factory', async () => {
    const result = 'result';
    const refreshResult = 'refreshResult';
    fnEvent.body.refreshToken = 'some-refresh-token';
    fnEvent.getSecret.mockResolvedValueOnce('1');
    fnEvent.getSecret.mockResolvedValueOnce('2');
    methods.exchangeRefreshToken = jest.fn().mockReturnValue(refreshResult);

    factory.mockResolvedValueOnce(result);

    expect(await handler(fnEvent, fnContext)).toBe(result);

    expect(factory.mock.calls[0].length).toBe(4);
    expect(factory.mock.calls[0][0]).toBe(fnEvent);
    expect(factory.mock.calls[0][1]).toBe(fnContext);
    expect(factory.mock.calls[0][2]).toBe(methods);

    expect(await factory.mock.calls[0][3]()).toBe(refreshResult);

    expect(methods.exchangeRefreshToken).toBeCalledWith('1', '2', fnEvent.body.refreshToken);
  });
});
