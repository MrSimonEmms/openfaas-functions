/**
 * handler.spec
 */

/* Node modules */
const { promises: fs } = require('fs');
const qs = require('querystring');

/* Third-party modules */
const axios = require('axios');

/* Files */
const distanceFinder = require('../../functions/distance-finder/handler');

const DISTANCE_API_URL = 'https://maps.googleapis.com/maps/api/distancematrix/json';

jest.mock('fs');

const mockedApiKey = 'some-api-key';

describe('distance-finder', () => {
  let context;
  beforeEach(() => {
    jest.resetModules();

    context = {
      headers: jest.fn().mockReturnThis(),
      status: jest.fn().mockReturnThis(),
      succeed: jest.fn(),
      fail: jest.fn(),
    };

    fs.readFile.mockRejectedValueOnce(new Error('unknown file'));
    fs.readFile.mockResolvedValue(mockedApiKey);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should throw error if invalid input entered', async () => {
    await distanceFinder(
      {
        body: {},
      },
      context,
    );

    const response = context.succeed.mock.calls[0][0];

    expect(response).toEqual({
      statusCode: 400,
      message: ['REQUIRED: origin', 'REQUIRED: destination'],
    });
  });

  it('should handle an API error', async () => {
    axios.get.mockResolvedValue({
      data: {
        status: 'INVALID_REQUEST',
      },
    });

    const body = {
      origin: 'some origin',
      destination: 'some dest',
    };

    await distanceFinder(
      {
        body,
      },
      context,
    );

    expect(axios.get).toBeCalledWith(
      `${DISTANCE_API_URL}?${qs.stringify({
        origins: body.origin,
        destinations: body.destination,
        mode: 'driving',
        language: 'en',
        units: 'metric',
        key: mockedApiKey,
      })}`,
    );

    const response = context.succeed.mock.calls[0][0];

    expect(response).toEqual({
      statusCode: 400,
      message: ['INVALID_REQUEST'],
    });
  });

  it('should handle an API error with an error_message', async () => {
    axios.get.mockResolvedValue({
      data: {
        status: 'INVALID_REQUEST',
        error_message: 'some error message',
      },
    });

    const body = {
      origin: 'some origin2',
      destination: 'some dest2',
    };

    await distanceFinder(
      {
        body,
      },
      context,
    );

    expect(axios.get).toBeCalledWith(
      `${DISTANCE_API_URL}?${qs.stringify({
        origins: body.origin,
        destinations: body.destination,
        mode: 'driving',
        language: 'en',
        units: 'metric',
        key: mockedApiKey,
      })}`,
    );

    const response = context.succeed.mock.calls[0][0];

    expect(response).toEqual({
      statusCode: 400,
      message: ['some error message'],
    });
  });

  it('should return default options - a return journey', async () => {
    axios.get
      .mockResolvedValueOnce({
        data: {
          destination_addresses: ['dest address'],
          origin_addresses: ['origin address'],
          rows: [
            {
              elements: [
                {
                  distance: {
                    text: '43.9 km',
                    value: 43903,
                  },
                  duration: {
                    text: '46 mins',
                    value: 2736,
                  },
                  status: 'OK',
                },
              ],
            },
          ],
          status: 'OK',
        },
      })
      .mockResolvedValueOnce({
        data: {
          destination_addresses: ['origin address'],
          origin_addresses: ['dest address'],
          rows: [
            {
              elements: [
                {
                  distance: {
                    text: '43.9 km',
                    value: 42900,
                  },
                  duration: {
                    text: '46 mins',
                    value: 2800,
                  },
                  status: 'OK',
                },
              ],
            },
          ],
          status: 'OK',
        },
      });

    const body = {
      origin: 'some origin3',
      destination: 'some dest3',
    };

    await distanceFinder(
      {
        body,
      },
      context,
    );

    expect(axios.get).toBeCalledTimes(2);
    expect(axios.get).toBeCalledWith(
      `${DISTANCE_API_URL}?${qs.stringify({
        origins: body.origin,
        destinations: body.destination,
        mode: 'driving',
        language: 'en',
        units: 'metric',
        key: mockedApiKey,
      })}`,
    );

    expect(axios.get).toBeCalledWith(
      `${DISTANCE_API_URL}?${qs.stringify({
        origins: body.destination,
        destinations: body.origin,
        mode: 'driving',
        language: 'en',
        units: 'metric',
        key: mockedApiKey,
      })}`,
    );

    const response = context.succeed.mock.calls[0][0];

    expect(response).toEqual({
      origin: 'origin address',
      destination: 'dest address',
      return: true,
      distance: 43903 + 42900,
      duration: 2736 + 2800,
    });
  });

  it('should return with set options - a single journey', async () => {
    axios.get.mockResolvedValueOnce({
      data: {
        destination_addresses: ['dest address2'],
        origin_addresses: ['origin address2'],
        rows: [
          {
            elements: [
              {
                distance: {
                  value: 40000,
                },
                duration: {
                  value: 5000,
                },
              },
            ],
          },
        ],
        status: 'OK',
      },
    });

    const body = {
      origin: 'some origin4',
      destination: 'some dest4',
      mode: 'walking',
      language: 'fr',
      return: false,
    };

    await distanceFinder(
      {
        body,
      },
      context,
    );

    expect(axios.get).toBeCalledTimes(1);
    expect(axios.get).toBeCalledWith(
      `${DISTANCE_API_URL}?${qs.stringify({
        origins: body.origin,
        destinations: body.destination,
        mode: body.mode,
        language: body.language,
        units: 'metric',
        key: mockedApiKey,
      })}`,
    );

    const response = context.succeed.mock.calls[0][0];

    expect(response).toEqual({
      origin: 'origin address2',
      destination: 'dest address2',
      return: false,
      distance: 40000,
      duration: 5000,
    });
  });
});
