/**
 * axios
 */

/* Node modules */

/* Third-party modules */

/* Files */

const axios = jest.fn();
axios.delete = jest.fn();
axios.get = jest.fn();
axios.post = jest.fn();

module.exports = axios;
