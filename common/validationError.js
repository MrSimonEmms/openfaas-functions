/**
 * validationError
 */

/* Node modules */

/* Third-party modules */

/* Files */

module.exports = class ValidationError extends Error {
  // eslint-disable-next-line class-methods-use-this
  get code() {
    return 'INVALID';
  }

  constructor(message, info = {}) {
    super(message);

    this.info = info;
  }
};
