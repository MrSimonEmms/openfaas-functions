# FreeAgent

Interact with the FreeAgent API

## Secrets

- `freeagent-client-id`: The FreeAgent Client ID
- `freeagent-client-secret`: The FreeAgent Client secret

## Getting a Refresh Token

Run `node ./functions/freeagent/token.js` to run a simple Express
server that gets a refresh token for your account and go to [localhost:3000](http://localhost:3000).
You will need to specify `CLIENT_ID` and `CLIENT_SECRET` as environment
variables with credentials from [FreeAgent](https://dev.freeagent.com/apps).

## Body

All endpoints require data in the following format:

```json
{
  "refreshToken": "...",
  "method": "method-name",
  "args": {}
}
```

### Methods

- addExpense
- getCurrentUser

### Args

Please populate the `args` object with the following key/value pairs.
Required values are marked with an asterisk (\*)

#### addExpense

Pass data into the `args` as required. See [FreeAgent](https://dev.freeagent.com/docs/expenses)
for data model.

#### getCurrentUser

This takes no `args`.
