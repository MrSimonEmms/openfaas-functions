/**
 * fs
 */

/* Node modules */

/* Third-party modules */

/* Files */

module.exports = {
  promises: {
    readFile: jest.fn(),
  },
};
