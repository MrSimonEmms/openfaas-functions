/**
 * methods
 */

/* Node modules */

/* Third-party modules */
const axios = require('axios');

/* Files */
const ValidationError = require('./common/validationError');

async function callApi(opts, token = null) {
  const config = {
    method: 'get',
    baseURL: process.env.FREEAGENT_URL || 'https://api.freeagent.com',
    headers: {
      'user-agent': 'OpenFaaS',
      'content-type': 'application/json',
      ...(opts.headers || {}),
    },
    ...opts,
  };

  if (token !== null) {
    config.headers.authorization = `Bearer ${token}`;
  }

  try {
    const { data } = await axios(config);

    return data;
  } catch (err) {
    throw new ValidationError('FreeAgent error', err.response.data);
  }
}

module.exports = {
  async addExpense(token, { expense }) {
    const data = {
      ...expense,
    };

    if (!expense.user) {
      /* Add in the current user */
      const { user } = await this.getCurrentUser(token);

      data.user = user.url;
    }

    return callApi(
      {
        data: {
          expense: data,
        },
        method: 'post',
        url: '/v2/expenses',
      },
      token,
    );
  },

  async exchangeRefreshToken(clientId, clientSecret, refreshToken) {
    const data = await callApi({
      url: '/v2/token_endpoint',
      method: 'post',
      data: {
        refresh_token: refreshToken,
        grant_type: 'refresh_token',
      },
      auth: {
        username: clientId,
        password: clientSecret,
      },
    });

    return data.access_token;
  },

  async getCurrentUser(token) {
    return callApi(
      {
        url: '/v2/users/me',
      },
      token,
    );
  },
};
