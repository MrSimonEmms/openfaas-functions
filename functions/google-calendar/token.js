/* istanbul ignore file */

/**
 * token
 *
 * Use this to generate a refresh token
 *
 * Get the client ID and secret from https://console.developers.google.com/apis/credentials
 */

/* eslint-disable import/no-extraneous-dependencies */

/* Node modules */

/* Third-party modules */
const express = require('express');
const passport = require('passport');
const { Strategy } = require('passport-google-oauth20');

/* Files */

const clientID = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;
const port = 3000;

const app = express();

const callbackUrl = '/callback';

passport.use(
  new Strategy(
    {
      clientID,
      clientSecret,
      callbackURL: `http://localhost:${port}${callbackUrl}`,
    },
    (token, refreshToken, profile, cb) => cb(null, refreshToken),
  ),
);

app
  .use(passport.initialize())
  .get(
    '/',
    passport.authenticate('google', {
      accessType: 'offline',
      scope: [
        'https://www.googleapis.com/auth/calendar.events',
        'https://www.googleapis.com/auth/calendar.readonly',
        'email',
      ],
    }),
  )
  .get(callbackUrl, passport.authenticate('google', { session: false }), (req, res) =>
    res.send({
      refreshToken: req.user,
    }),
  );

app.listen(port, () => {
  console.log(`Listening on http://localhost:${port}`);
});
