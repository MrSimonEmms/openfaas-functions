/**
 * validationError.spec
 */

/* Node modules */

/* Third-party modules */

/* Files */
const ValidationError = require('../../common/validationError');

describe('validationError', () => {
  it('should extend the validation error and no info', () => {
    const msg = 'some-error';
    const err = new ValidationError(msg);

    expect(err).toBeInstanceOf(ValidationError);
    expect(err).toBeInstanceOf(Error);

    expect(err.message).toBe(msg);
    expect(err.code).toBe('INVALID');
    expect(err.info).toEqual({});
  });

  it('should extend the validation error and some info object', () => {
    const msg = 'some-error2';
    const info = {
      hello: 'world',
    };
    const err = new ValidationError(msg, info);

    expect(err).toBeInstanceOf(ValidationError);
    expect(err).toBeInstanceOf(Error);

    expect(err.message).toBe(msg);
    expect(err.code).toBe('INVALID');
    expect(err.info).toBe(info);
  });
});
