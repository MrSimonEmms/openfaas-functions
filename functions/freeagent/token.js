/* istanbul ignore file */

/**
 * token
 *
 * Use this to generate a refresh token
 *
 * Get the client ID and secret from https://dev.freeagent.com/apps
 */

/* eslint-disable import/no-extraneous-dependencies */

/* Node modules */

/* Third-party modules */
const express = require('express');
const passport = require('passport');
const { Strategy } = require('passport-oauth2');

/* Files */

const clientID = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;
const freeagentUrl = process.env.FREEAGENT_URL || 'https://api.freeagent.com';
const port = 3000;

const app = express();

const callbackUrl = '/callback';

passport.use(
  new Strategy(
    {
      authorizationURL: `${freeagentUrl}/v2/approve_app`,
      tokenURL: `${freeagentUrl}/v2/token_endpoint`,
      clientID,
      clientSecret,
      callbackURL: `http://localhost:${port}${callbackUrl}`,
    },
    (token, refreshToken, profile, cb) => cb(null, refreshToken),
  ),
);

app
  .use(passport.initialize())
  .get('/', passport.authenticate('oauth2', {}))
  .get(callbackUrl, passport.authenticate('oauth2', { session: false }), (req, res) =>
    res.send({
      refreshToken: req.user,
    }),
  );

app.listen(port, () => {
  console.log(`Listening on http://localhost:${port}`);
});
