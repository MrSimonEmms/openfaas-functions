# Distance Finder

Calculate the distance between two map points, calling the [Distance Matrix](https://developers.google.com/maps/documentation/distance-matrix/start)

## Secrets

- `google-maps-api-key`: The [Google API key](https://developers.google.com/maps/documentation/javascript/get-api-key)

## Body

See [Google API docs](https://developers.google.com/maps/documentation/distance-matrix/intro#required-parameters)
for data types.

- `origin` \<string\>: The origin point - typically a postal code, town or lat/long **REQUIRED**
- `destination` \<string\>: The destination point - as `origin` **REQUIRED**
- `mode` \<string\>: Defaults to `driving`
- `language` \<string>: Defaults to `en`
- `return` \<bool\>: Is this a return journey? Defaults to `true`

## Output

- `distance` \<number\>: Distance in metres
- `duration` \<number\>: Estimated duration in seconds
- `origin` \<string>\: Resolved origin
- `destination` \<string\>: Resolved destination
- `return` \<bool\>: Is this a return journey?

## Error
